<?php

function my_library_status_form($form, $form_state){
  $form = array();
  global $user;
  $current_user = $user->uid;

  //definizione colonne
  $header = array(
    'id' => t('ID'),
    'title' => t('Title'),
    'author' => t('Author'),
    'isbn' => t('ISBN'),
    'status' => t('Status'),
  );

  $query = db_select('node', 'n')->extend('PagerDefault');
  $query->join('field_data_field_author', 'fa', 'n.nid = fa.entity_id');
  $query->join('field_data_field_isbn', 'fi', 'n.nid = fi.entity_id');
  $query->join('field_data_field_status', 'fs', 'n.nid = fs.entity_id');

  //controlla se l'utente corrente è il destinatario della notifica
  $query->condition('n.uid', $current_user);

  $query
    ->fields('n', array('nid','title'))
    ->fields('fa', array('field_author_value'))
    ->fields('fi', array('field_isbn_value'))
    ->fields('fs', array('field_status_tid'))
    ->groupBy('n.nid')
    ->orderBy('nid', 'ASC')
    ->limit(25);

  $results = $query->execute();

  $rows = array();

  foreach ($results as $result){
    $rows[] = array(
      'id' => $result->nid,
      'title' => l($result->title, 'node/' . $result->nid),
      'author' => $result->field_author_value,
      'isbn' => $result->field_isbn_value,
      'status' => taxonomy_term_load($result->field_status_tid)->name,
      '#attributes' => array('class'=> array('application-row')),
    );
  }

  $form['book_status_action']['bookable'] = array(
    '#type' => 'select',
    '#title' => 'Actions',
    '#options' => array(
      1 => t('Bookable'),
      2 => t('Not Bookable'),
    ),
  );

  $form['book_status_table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $rows,
    '#empty' => t('No books found'),
    '#multiple' => FALSE, //false = radio, true = checkbox
    '#attributes' => array('class'=> array('application-row')),
  );

  $form['pager'] = array('#markup' => theme('pager'));

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function my_library_status_form_submit($form, $form_state){
  $action = $form_state['values']['bookable'];
  $values = array();
  $value = $form_state['values']['book_status_table'];
  $values[] = $form_state['complete form']['book_status_table']['#options'][$value];
  change_status($action, $values);
}

function change_status($action, $values){
  if($values[0]['status'] == 'Loaned'){
    $book_id = $values[0]['id'];
    $query = db_select('field_data_field_book_loaned', 'bl');
    $query->leftJoin('field_data_field_date_to', 'dt', 'bl.entity_id = dt.entity_id');
    $query->isNull('dt.entity_id');
    $query->condition('bl.field_book_loaned_target_id', $book_id);
    $query->fields('bl', array('entity_id',) );

    $results = $query->execute();

    foreach($results as $result){
      //dsm($result);
      $nid = db_insert('field_data_field_date_to')
        ->fields(array(
          'entity_type' => 'node',
          'bundle' => 'bibliotita_book_loan',
          'deleted' => 0,
          'entity_id' => $result->entity_id,
          'revision_id' => $result->entity_id,
          'language' => 'und',
          'delta' => 0,
          'field_date_to_value' => time(),
        ))
        ->execute();
    }
  }

  $action = ($action == 1 ? 'Bookable' : 'Not Bookable');
  $terms = taxonomy_get_term_by_name($action);
  foreach($terms as $term){
    $status = $term->tid;
  }
  db_update('field_data_field_status')
    ->fields(array(
      'field_status_tid' => $status,
    ))
    ->condition('entity_id', $values[0]['id'])
    ->execute();

  if($action == 'Not Bookable'){
    deleteAllBookNotification($values[0]['id']);
  }
  drupal_flush_all_caches();

  check_number_of_bookables();
}

function check_number_of_bookables(){
  global $user;
  $query = db_select('field_data_field_status', 'fs');
  $query->join('node', 'n', 'n.nid = fs.entity_id');
  $query->condition('n.uid', $user->uid);
  $query->condition('n.type', 'bibliotita_book');
  $bookable_terms= taxonomy_get_term_by_name('Bookable');
  foreach($bookable_terms as $term){
    $bookable_tid = $term->tid;
  }
  $bookable_terms= taxonomy_get_term_by_name('Loaned');
  foreach($bookable_terms as $term){
    $loaned_tid = $term->tid;
  }
  $or = db_or();
  $or->condition('fs.field_status_tid', $bookable_tid);
  $or->condition('fs.field_status_tid', $loaned_tid);
  $query->condition($or);
  $query->fields('n', array('nid'));

  $results = $query->execute();

  $bookables = 0;
  foreach ($results as $result){
    $bookables++;
  }

  if($bookables >= 1){
    remove_role_from_user($user, 'not_bibliotita_user');
    add_role_to_user($user, 'bibliotita_user');
  }else{
    remove_role_from_user($user, 'bibliotita_user');
    add_role_to_user($user, 'not_bibliotita_user');
  }
}

function deleteAllBookNotification($book_id){
  global $user;
  $query = db_select('bibliotita_notifications', 'bn');
  $query->condition('book_id', $book_id);
  $query->fields('bn', array('id', 'applicant_owner', 'type'));

  $results = $query->execute();

  foreach ($results as $result){
    $recipient = $result->applicant_owner;
    if($result->type == 1){
      createNotification($book_id, $recipient, 2);
    }
    db_delete('bibliotita_notifications')
      ->condition('id', $result->id)
      ->condition('type', 1)
      ->execute();
  }

  db_delete('flagging')
    ->condition('entity_id', $book_id)
    ->execute();
}

function createNotification($book_id, $recipient_id, $action){
  global $user;
  $applicant_owner = $user->uid;
  $status = ($action == 1) ? 'approved' : 'denied';
  $nid = db_insert('bibliotita_notifications')
    ->fields(array(
      'flagging_id' => -1,
      'timestamp' => time(),
      'recipient_id' => $recipient_id,
      'message' => 'Your application has been ' . $status,
      'book_id' => $book_id,
      'applicant_owner' => $applicant_owner,
      'type' => 2,
    ))
    ->execute();
}

function add_role_to_user($user, $role_name) {
  // For convenience, we'll allow user ids as well as full user objects.
  if (is_numeric($user)) {
    $user = user_load($user);
  }
  // If the user doesn't already have the role, add the role to that user.
  $key = array_search($role_name, $user->roles);
  if ($key == FALSE) {
    // Get the rid from the roles table.
    $roles = user_roles(TRUE);
    $rid = array_search($role_name, $roles);
    if ($rid != FALSE) {
      $new_role[$rid] = $role_name;
      $all_roles = $user->roles + $new_role; // Add new role to existing roles.
      user_save($user, array('roles' => $all_roles));
    }
  }
}

function remove_role_from_user($user, $role_name) {
  // For convenience, we'll allow user ids as well as full user objects.
  if (is_numeric($user)) {
    $user = user_load($user);
  }
  // Only remove the role if the user already has it.
  $key = array_search($role_name, $user->roles);
  if ($key == TRUE) {
    // Get the rid from the roles table.
    $roles = user_roles(TRUE);
    $rid = array_search($role_name, $roles);
    if ($rid != FALSE) {
      // Make a copy of the roles array, without the deleted one.
      $new_roles = array();
      foreach($user->roles as $id => $name) {
        if ($id != $rid) {
          $new_roles[$id] = $name;
        }
      }
      user_save($user, array('roles' => $new_roles));
    }
  }
}
