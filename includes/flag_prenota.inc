<?php

function notifications_form($form, $form_state){
  $form = array();
  global $user;
  $current_user = $user->uid;

  //definizione colonne
  $header = array(
    'id' => t('ID'),
    'timestamp' => t('Timestamp'),
    'message' => t('Messaggio'),
    'book' => t('Libro'),
    'applicant_owner' => t('Richiedente/Proprietario'),
  );

  $query = db_select('bibliotita_notifications', 'bn')->extend('PagerDefault');
  $query->join('node', 'n', 'n.nid = bn.book_id');
  $query->join('users', 'u', 'u.uid = bn.applicant_owner');

  //controlla se l'utente corrente è il destinatario della notifica
  $query->condition('bn.recipient_id', $current_user);

  $query
    ->fields('bn', array('id','timestamp', 'message', 'book_id'))
    ->fields('n', array('title'))
    ->fields('u', array('name', 'uid'))
    ->orderBy('timestamp', 'DESC')
    ->limit(25);

  $results = $query->execute();

  $rows = array();

  foreach ($results as $result){

    $rows[] = array(
      'id' => $result->id,
      'timestamp' => format_date($result->timestamp),
      'message' => $result->message,
      'book' => l($result->title, 'node/' . $result->book_id),
      'applicant_owner' => l($result->name, 'user/' . $result->uid),
      '#attributes' => array('class'=> array('application-row')),
    );
  }

  $form['flag_prenota_action']['approvedeny'] = array(
    '#type' => 'select',
    '#title' => 'Actions',
    '#options' => array(
      1 => t('Approve'),
      2 => t('Deny'),
      3 => t('Delete'),
    ),
  );

  $form['flag_prenota_table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $rows,
    '#empty' => t('No applications found'),
    '#multiple' => FALSE,
    '#attributes' => array('class'=> array('application-row')),
  );

  $form['pager'] = array('#markup' => theme('pager'));

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function notifications_form_validate($form, $form_state){
  $action = $form_state['values']['approvedeny'];
  $values = array();
  $value = $form_state['values']['flag_prenota_table'];
  $values[] = $form_state['complete form']['flag_prenota_table']['#options'][$value];
  $result = db_select('bibliotita_notifications', 'bn')
    ->fields('bn', array('type',))
    ->condition('id', $values[0]['id'])
    ->execute()
    ->fetchAssoc();
  $type = $result['type'];
  if (
      ( ($type == 1) && ($action == 3) ) ||
      ( ($type == 2) && ($action != 3) )
    )
    {
    form_set_error('approvedeny', "Action not valid for the selected notification");
  }
}

function notifications_form_submit($form, $form_state){
  $action = $form_state['values']['approvedeny'];
  $values = array();
  $value = $form_state['values']['flag_prenota_table'];
  $values[] = $form_state['complete form']['flag_prenota_table']['#options'][$value];
  flag_prenota_administration($action, $values);
}

function flag_prenota_administration($action, $values){

  foreach ($values as $key => $value) {
    $result = db_select('bibliotita_notifications', 'bn')
      ->fields('bn', array('flagging_id',))
      ->condition('id', $value['id'])
      ->execute()
      ->fetchAssoc();
    $flagging_id = $result['flagging_id'];
    //ottengo uid e nid dalla tabella flagging
    $result = db_select('flagging', 'f')
      ->fields('f', array('entity_id', 'uid'))
      ->condition('flagging_id', $flagging_id)
      ->execute()
      ->fetchAssoc();
    $book_id = $result['entity_id'];
    $rentee_id = $result['uid'];

    switch($action){
      case 1:
        createLoan($rentee_id, $book_id);
        deleteBookNotification($book_id, $rentee_id);
        createNotification($book_id, $result['uid'], $action);
        //TODO: cambiare stato libro in prestato
        break;
      case 2:
        deleteNotification($value['id'], $flagging_id);
        createNotification($book_id, $result['uid'], $action);
        break;
      case 3:
        deleteNotification($value['id'], $flagging_id);
        break;
    }

  }
}

function createLoan($rentee_id, $book_id) {
  $action = 'Loaned';
  $terms = taxonomy_get_term_by_name($action);
  foreach($terms as $term){
    $status = $term->tid;
  }
  db_update('field_data_field_status')
    ->fields(array(
      'field_status_tid' => $status,
    ))
    ->condition('entity_id', $book_id)
    ->execute();
  drupal_flush_all_caches();

  global $user;
  $book_owner_id = $user->uid;
  $book_owner_name = $user->name;
  $node = new stdClass();
  $node->title = "Book loan";
  $node->type = "bibliotita_book_loan";
  //$node->body = l('link', 'node/' . $book_id);
  node_object_prepare($node); // Sets some defaults. Invokes hook_prepare() and hook_node_prepare().
  $node->language = LANGUAGE_NONE; // Or e.g. 'en' if locale is enabled
  $node->uid = $book_owner_id;
  $node->status = 1; //(1 or 0): published or not
  $node->promote = 0; //(1 or 0): promoted to front page
  $node->comment = 0; // 0 = comments disabled, 1 = read only, 2 = read/write


  $node->field_book_owner[$node->language][]['target_id'] = $book_owner_id;
  $node->field_rentee[$node->language][]['target_id'] = $rentee_id;
  $node->field_book_loaned[$node->language][]['target_id'] = $book_id;
  $node->field_date_from[$node->language][0]['value'] = time();

  node_object_prepare($node);
  $node = node_submit($node); // Prepare node for saving
  node_save($node);
  //drupal_set_message( "Node with nid " . $node->nid . " saved!\n");
  $form_state['redirect']  = 'bibliotita_notifications';
}

function deleteBookNotification($book_id, $rentee_id){

  $query = db_select('bibliotita_notifications', 'bn');
  $query->condition('book_id', $book_id);
  $query->fields('bn', array('id', 'applicant_owner', 'type'));

  $results = $query->execute();

  foreach ($results as $result){
    $recipient = $result->applicant_owner;
    if( ($recipient != $rentee_id) &&
        ($result->type == 1))
    {
      createNotification($book_id, $recipient, 2);
    }
    db_delete('bibliotita_notifications')
      ->condition('id', $result->id)
      ->condition('type', 1)
      ->execute();
  }

  db_delete('flagging')
    ->condition('entity_id', $book_id)
    ->execute();
}


function createNotification($book_id, $recipient_id, $action){
  global $user;
  $applicant_owner = $user->uid;
  $status = ($action == 1) ? 'approved' : 'denied';
  $nid = db_insert('bibliotita_notifications')
    ->fields(array(
      'flagging_id' => -1,
      'timestamp' => time(),
      'recipient_id' => $recipient_id,
      'message' => 'Your application has been ' . $status,
      'book_id' => $book_id,
      'applicant_owner' => $applicant_owner,
      'type' => 2,
    ))
    ->execute();
}

function deleteNotification($notification_id, $flagging_id){
  db_delete('bibliotita_notifications')
    ->condition('id', $notification_id)
    ->execute();

  db_delete('flagging')
    ->condition('flagging_id', $flagging_id)
    ->execute();
}
